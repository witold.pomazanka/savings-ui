# Savings UI

Prerequisites: [nodejs]

## Development

1) Execute command in cmd/PowerShell - in source directory: `npm install`    
2) Execute command in cmd/PowerShell - in source directory: `npm start`    
3) Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.


## Known issues

If error saying `Error: Node Sass does not yet support your current environment: Windows 64-bit with Unsupported runtime (72)`
 will appear, then run command in `front` directory: `npm rebuild node-sass`

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build-prod` to build the project. The build artifacts will be stored in the `dist/` directory.

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).

[nodejs]: https://nodejs.org/en/download/