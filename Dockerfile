FROM nginx:stable-alpine

COPY /dist/savings /usr/share/nginx/html
COPY /config/nginx.conf /etc/nginx