import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot} from '@angular/router';
import {Observable} from 'rxjs';
import {AuthenticationService} from "./authentication.service";

@Injectable()
export class LoginGuardService implements CanActivate {

  constructor(private router: Router, private authenticationService: AuthenticationService) {
  }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> {
    return new Observable<boolean>(observer => {
      this.authenticationService.isNotAuthenticated().subscribe(response => {
        if (response.valueOf()) {
          return observer.next(true);
        } else {
          this.router.navigate(['/']);
          return observer.next(false);
        }
      });
    });
  }
}
