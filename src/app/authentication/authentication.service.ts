import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Router} from '@angular/router';

@Injectable({
    providedIn: 'root'
})
export class AuthenticationService {

    username: string;
    authenticated: boolean;
    authorities: Array<string>;

    constructor(private http: HttpClient, private router: Router) {
    }

    create(credentials): void {
        this.username = credentials["username"];
        this.authenticated = credentials["enabled"];
        this.authorities = credentials["authorities"];
    }

    destroy(): void {
        this.http.post('api/logout', {})
            .subscribe(response => {
                this.username = null;
                this.authenticated = false;
                this.authorities = null;
                this.router.navigate(['/login']);
            });
    }

    isAuthenticated(): Observable<boolean> {
        return new Observable<boolean>(observer => {
            if (this.authenticated === undefined) {
                this.fetchAuthenticationData().subscribe(response => {
                    if (response) {
                        this.create(response);
                        observer.next(this.authenticated);
                    } else {
                        this.authenticated = false;
                        observer.next(this.authenticated)
                    }
                });
            } else {
                observer.next(this.authenticated);
            }
        });
    }

    isNotAuthenticated(): Observable<boolean> {
        return new Observable<boolean>(observer => {
            this.isAuthenticated().subscribe(response => {
                return observer.next(!response.valueOf());
            });
        });
    }

    fetchAuthenticationData(): Observable<Object> {
        return this.http.get("/api/user/get-authentication");
    }
}
