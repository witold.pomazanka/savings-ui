import {Component, OnInit} from '@angular/core';
import {ExpenditureService} from '../expenditure/expenditure.service';
import {Expenditure} from '../expenditure/expenditure';
import {Category} from '../category/category';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import * as moment from 'moment';
import {Moment} from 'moment';
import {ActivatedRoute} from '@angular/router';

@Component({
    selector: 'app-dashboard',
    templateUrl: './dashboard.component.html',
    styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

    form: FormGroup;
    expendituresGroups: Map<Category, Expenditure[]>;
    isDisabled = true;

    constructor(private expenditureService: ExpenditureService,
                private route: ActivatedRoute) {
    }

    ngOnInit() {
        this.initializeForm();
        this.fetch();
    }

    initializeForm(): void {
        this.form = new FormBuilder().group({
            'since': new FormControl(this.determineSinceDate(), Validators.required),
            'until': new FormControl(this.determineUntilDate(), Validators.required)
        });
    }

    fetch(): void {
        const since = this.form.controls['since'].value;
        const until = this.form.controls['until'].value;
        this.expenditureService.findAllGroupByCategory(since, until)
            .subscribe(data => this.expendituresGroups = data,
                () => {
                },
                () => this.isDisabled = false);
    }

    deleteExpenditure(id: number): void {
        this.isDisabled = true;
        this.expenditureService.delete(id).subscribe(response => {
            window.location.reload();
        });
    }

    requestPending(): boolean {
        return this.isDisabled;
    }

    private determineSinceDate() {
        const date = this.getAppropriateDateFromQueryParam('since');
        return date ? date : moment().startOf('month');
    }

    private determineUntilDate() {
        const date = this.getAppropriateDateFromQueryParam('until');
        return date ? date : moment().endOf('month');
    }

    private getAppropriateDateFromQueryParam(paramName: string): Moment {
        const params = this.route.snapshot.paramMap;
        if (!!params.keys.length) {
            return moment(params[paramName]);
        }
    }
}
