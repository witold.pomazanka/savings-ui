import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {HttpClient} from '@angular/common/http';
import {Label} from './label';
import {AddLabel} from './add.label';

@Injectable({
    providedIn: 'root'
})
export class LabelService {
    private baseUrl = 'api/label';

    constructor(private httpClient: HttpClient) {
    }

    getOne(id: string): Observable<Label> {
        return this.httpClient.get<Label>(`${this.baseUrl}/${id}`);
    }

    findAll(): Observable<Label[]> {
        return this.httpClient.get<Label[]>(`${this.baseUrl}`);
    }

    add(data: AddLabel): Observable<Label> {
        return this.httpClient.post<Label>(`${this.baseUrl}`, data);
    }

    delete(id: number) {
        return this.httpClient.delete(`${this.baseUrl}/${id}`);
    }

    update(data: Label) {
        return this.httpClient.put(`${this.baseUrl}/${data.id}`, data);
    }
}
