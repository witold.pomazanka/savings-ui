import {Component, OnInit} from '@angular/core';
import {LabelService} from "./label.service";
import {Label} from "./label";
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {AddLabel} from "./add.label";
import {ActivatedRoute, Router} from "@angular/router";

@Component({
    selector: 'app-label',
    templateUrl: './label.component.html',
    styleUrls: ['./label.component.css']
})
export class LabelComponent implements OnInit {

    labels: Label[];
    form: FormGroup;

    constructor(private service: LabelService, private route: ActivatedRoute, private router: Router) {
    }

    ngOnInit() {
        this.initializeForm();
        this.findAll();
        this.findLabelToEditIfDefined();
    }

    submit(): void {
        if (!!this.form.controls.id.value) {
            this.update()
        } else {
            this.add();
        }

    }

    delete(id: number): void {
        this.service.delete(id).subscribe(response => {
            this.findAll();
        });
    }

    getSubmitButtonLabel(): string {
        return !!this.getPathId() ? 'Submit' : 'Add';
    }

    edit(id: number) {
        this.router.navigate([`/label/${id}`]).then(() => {
            if (!!this.getPathId()) {
                window.location.reload();
            }
        });
    }

    private findLabelToEditIfDefined() {
        const id = this.getPathId();
        if (id) {
            this.service.getOne(id).subscribe(response => {
                this.initializeForm(response);
            })
        }
    }

    private getPathId() {
        return this.route.snapshot.paramMap.get('id');
    }

    private initializeForm(dto?: Label): void {
        this.form = new FormGroup({
            "id": new FormControl(dto?.id),
            "name": new FormControl(dto?.name, Validators.required)
        });
    }

    private findAll(): void {
        this.service.findAll().subscribe(response => {
            this.labels = response;
        });
    }

    private add(): void {
        this.service
            .add(new AddLabel(this.form.controls.name.value))
            .subscribe(() => {
                this.form.reset();
                this.findAll();
            });
    }

    private update(): void {
        this.service
            .update(this.form.value)
            .subscribe(() => {
                this.router.navigate(['/label']);
            });
    }
}
