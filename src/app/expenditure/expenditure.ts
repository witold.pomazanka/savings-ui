import {Category} from '../category/category';

export class Expenditure {
    id: number;
    price: string;
    name: string;
    category: Category;
    eventDate: string;
    comment: string;
    labels: string[];
    income: boolean;
    account: Account;
}
