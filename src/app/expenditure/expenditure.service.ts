import {Injectable} from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Expenditure} from './expenditure';
import {AddExpenditure} from './add.expenditure';
import {Category} from '../category/category';
import {Moment} from 'moment';

@Injectable({
    providedIn: 'root'
})
export class ExpenditureService {
    private baseUrl = 'api/expenditure';

    dateFormat = 'YYYY-MM-DD';

    constructor(private httpClient: HttpClient) {
    }

    findAllGroupByCategory(since: Moment, until: Moment): Observable<Map<Category, Expenditure[]>> {
        const params = new HttpParams()
            .append('since', since.format(this.dateFormat))
            .append('until', until.format(this.dateFormat));
        return this.httpClient.get <Map<Category, Expenditure[]>>(`${this.baseUrl}/groups`, {params: params});
    }

    getOne(id: string): Observable<Expenditure> {
        return this.httpClient.get<Expenditure>(`${this.baseUrl}/${id}`);
    }

    add(data: AddExpenditure): Observable<Expenditure> {
        return this.httpClient.post<Expenditure>(`${this.baseUrl}`, data);
    }

    delete(id: number) {
        return this.httpClient.delete<Expenditure>(`${this.baseUrl}/${id}`);
    }

    update(pathId: string, data: AddExpenditure) {
        return this.httpClient.put<Expenditure>(`${this.baseUrl}/${pathId}`, data);
    }
}
