import {Category} from "../category/category";

export class AddExpenditure {
    price: number;
    name: string;
    categories: Category[];
}
