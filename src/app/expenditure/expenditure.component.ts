import {Component, OnInit} from '@angular/core';
import {Expenditure} from './expenditure';
import {ExpenditureService} from './expenditure.service';
import {CategoryService} from '../category/category.service';
import {Category} from '../category/category';
import {Account} from '../account/account';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';
import {Label} from '../label/label';
import {LabelService} from '../label/label.service';
import {AccountService} from '../account/account.service';
import {forkJoin, Observable} from 'rxjs';


@Component({
    selector: 'app-expenditure',
    templateUrl: './expenditure.component.html',
    styleUrls: ['./expenditure.component.css']
})
export class ExpenditureComponent implements OnInit {

    newEntryForm: FormGroup;
    allCategories: Category[];
    allLabels: Label[];
    isDisabled = false;
    account: Account;

    constructor(private service: ExpenditureService,
                private categoryService: CategoryService,
                private labelService: LabelService,
                private accountService: AccountService,
                private fb: FormBuilder,
                private route: ActivatedRoute,
                private router: Router) {
    }

    ngOnInit() {
        this.initializeForm();
        this.prepareForm();
    }

    private prepareForm() {
        this.fetchData().subscribe(responseList => {
            this.account = responseList[0];
            this.setDefaultAccountIdFormField();
            this.allLabels = responseList[1];
            this.allCategories = responseList[2];
            this.findExpenditureToEditIfDefined();
            this.setFocusOnPriceField();
        });
    }

    initializeForm(dto?: Expenditure): void {
        this.newEntryForm = this.fb.group({
            'id': new FormControl(dto?.id),
            'income': new FormControl(dto?.income || false),
            'price': new FormControl(dto?.price, [Validators.required, Validators.min(-99999), Validators.max(99999)]),
            'name': new FormControl(dto?.name, Validators.required),
            'category': new FormControl(dto?.category?.id),
            'eventDate': new FormControl(dto?.eventDate),
            'labels': new FormControl(dto?.labels),
            'comment': new FormControl(dto?.comment),
            'account': new FormControl(dto?.account.id)
        });
        this.setFocusOnPriceField();
    }

    private findExpenditureToEditIfDefined(): void {
        const id = this.getPathId();
        if (id) {
            this.service.getOne(id).subscribe(response => {
                this.initializeForm(response);
            });
        }
    }

    submit(): void {
        this.isDisabled = true;
        if (!!this.getPathId()) {
            this.update();
        } else {
            this.add();
        }
    }

    add(): void {
        this.service.add(this.newEntryForm.value).subscribe(response => {
            this.ngOnInit();
        }, () => {
        }, () => {
            this.isDisabled = false;
            this.setFocusOnPriceField();
        });
    }

    update(): void {
        this.service.update(this.getPathId(), this.newEntryForm.value).subscribe(response => {
            this.router.navigate([`/expenditure`]);
            this.ngOnInit();
        }, () => {
        }, () => {
            this.isDisabled = false;
            this.setFocusOnPriceField();
        });
    }

    setFocusOnPriceField(): void {
        setTimeout(() => {
            document.getElementById('price').focus();
        });
    }

    replaceCommaWithDot() {
        this.newEntryForm.controls.price.setValue(this.newEntryForm.controls.price.value?.toString().replace(',', '.'));
    }

    requestPending(): boolean {
        return this.isDisabled;
    }

    getSubmitButtonLabel(): string {
        return !!this.getPathId() ? 'Submit' : 'Add';
    }

    private getPathId() {
        return this.route.snapshot.paramMap.get('id');
    }

    private fetchData(): Observable<[Account, Label[], Category[]]> {
        const defaultAccountResponse = this.accountService.getDefaultAccount();
        const labelsResponse = this.labelService.findAll();
        const categoryResponse = this.categoryService.findAll();
        return forkJoin(defaultAccountResponse, labelsResponse, categoryResponse);
    }

    private setDefaultAccountIdFormField() {
        this.newEntryForm.controls['account'].setValue(this.account.id);
    }
}
