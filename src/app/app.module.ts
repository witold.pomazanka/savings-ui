import {BrowserModule} from '@angular/platform-browser';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {NgModule} from '@angular/core';

import {AppComponent} from './app.component';
import {NavbarComponent} from './navbar/navbar.component';
import {HttpClientModule} from '@angular/common/http';
import {ExpenditureComponent} from './expenditure/expenditure.component';
import {AppRoutingModule} from './app-routing.module';
import {LoginComponent} from './login/login.component';
import {FilterPipeModule} from 'ngx-filter-pipe';
import {NgSelectModule} from '@ng-select/ng-select';
import {CategoryComponent} from './category/category.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {MatDatepickerModule} from '@angular/material/datepicker';
import {DateAdapter, MAT_DATE_FORMATS} from '@angular/material/core';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatInputModule} from '@angular/material/input';
import {DragDropModule} from '@angular/cdk/drag-drop';
import {LogoutComponent} from './logout/logout.component';
import {DashboardComponent} from './dashboard/dashboard.component';
import {MAT_MOMENT_DATE_ADAPTER_OPTIONS, MatMomentDateModule} from '@angular/material-moment-adapter';
import {LabelComponent} from './label/label.component';
import {CustomDateAdapter} from './config/custom.date.adapter';
import {DisableControlDirective} from './helpers/disable.control.directive';
import {StatisticsComponent} from './statistics/statistics.component';
import {MatButtonModule} from '@angular/material/button';
import {AccountComponent} from './account/account.component';


@NgModule({
    declarations: [
        AppComponent,
        NavbarComponent,
        ExpenditureComponent,
        LoginComponent,
        CategoryComponent,
        LabelComponent,
        LogoutComponent,
        DashboardComponent,
        DisableControlDirective,
        StatisticsComponent,
        AccountComponent
    ],
    imports: [
        BrowserModule,
        FormsModule,
        HttpClientModule,
        AppRoutingModule,
        ReactiveFormsModule,
        FilterPipeModule,
        NgSelectModule,
        BrowserAnimationsModule,
        MatDatepickerModule,
        MatMomentDateModule,
        MatInputModule,
        MatButtonModule,
        MatFormFieldModule,
        DragDropModule
    ],
    providers: [
        {provide: DateAdapter, useClass: CustomDateAdapter},
        {provide: MAT_MOMENT_DATE_ADAPTER_OPTIONS, useValue: {useUtc: true}},
        {
            provide: MAT_DATE_FORMATS,
            useValue: {
                parse: {
                    dateInput: 'DD MM YYYY',
                },
                display: {
                    dateInput: 'DD-MM-YYYY',
                    monthYearLabel: 'MMM YYYY',
                    dateA11yLabel: 'DD-MM-YYYY',
                    monthYearA11yLabel: 'MMM YYYY',
                }
            }
        }
    ],
    bootstrap: [AppComponent]
})
export class AppModule {
}
