import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {LoginComponent} from './login/login.component';
import {AuthGuardService} from './authentication/auth.guard.service';
import {LoginGuardService} from './authentication/login.guard.service';
import {ExpenditureComponent} from './expenditure/expenditure.component';
import {CategoryComponent} from './category/category.component';
import {LogoutComponent} from './logout/logout.component';
import {DashboardComponent} from './dashboard/dashboard.component';
import {LabelComponent} from './label/label.component';
import {StatisticsComponent} from './statistics/statistics.component';
import {AccountComponent} from './account/account.component';

const routes: Routes = [
    {path: 'login', component: LoginComponent, canActivate: [LoginGuardService]},
    {path: 'statistics', component: StatisticsComponent, canActivate: [AuthGuardService]},
    {path: 'dashboard', component: DashboardComponent, canActivate: [AuthGuardService]},
    {path: 'logout', component: LogoutComponent, canActivate: [AuthGuardService]},
    {path: 'expenditure', component: ExpenditureComponent, canActivate: [AuthGuardService]},
    {path: 'expenditure/:id', component: ExpenditureComponent, canActivate: [AuthGuardService]},
    {path: 'account', component: AccountComponent, canActivate: [AuthGuardService]},
    {path: 'account/:id', component: AccountComponent, canActivate: [AuthGuardService]},
    {path: 'category', component: CategoryComponent, canActivate: [AuthGuardService]},
    {path: 'label', component: LabelComponent, canActivate: [AuthGuardService]},
    {path: 'label/:id', component: LabelComponent, canActivate: [AuthGuardService], runGuardsAndResolvers: 'always'},
    {
        path: '', redirectTo: '/expenditure', pathMatch: 'full', canActivate: [AuthGuardService]
    },
    {
        path: '**', redirectTo: '/expenditure', pathMatch: 'full', canActivate: [AuthGuardService]
    }
];

@NgModule({
    imports: [RouterModule.forRoot(routes, {onSameUrlNavigation: 'reload'})],
    exports: [RouterModule],
    providers: [AuthGuardService, LoginGuardService]
})

export class AppRoutingModule {
}
