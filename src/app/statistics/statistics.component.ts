import {Component, OnInit} from '@angular/core';
import {StatisticsService} from './statistics.service';
import {ActivatedRoute} from '@angular/router';
import * as moment from 'moment';
import {Moment} from 'moment';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {AccountService} from '../account/account.service';
import {Account} from '../account/account';
import {StatisticsSummary} from './statistic.summary';

@Component({
    selector: 'app-statistics',
    templateUrl: './statistics.component.html'
})
export class StatisticsComponent implements OnInit {

    statisticsForm: FormGroup;
    statisticsSummary: StatisticsSummary;
    account: Account;
    isDisabled = true;

    constructor(private route: ActivatedRoute,
                private statisticsService: StatisticsService,
                private accountService: AccountService) {
    }

    ngOnInit() {
        this.getDefaultAccount();
        this.initializeForm();
        this.fetchStatistics();
    }

    initializeForm(): void {
        this.statisticsForm = new FormBuilder().group({
            'since': new FormControl(this.determineSinceDate(), Validators.required),
            'until': new FormControl(this.determineUntilDate(), Validators.required)
        });
    }

    fetchStatistics(): void {
        const since = this.statisticsForm.controls['since'].value;
        const until = this.statisticsForm.controls['until'].value;
        this.statisticsService.findCategoriesWithLabelsAndTheirSumsForGivenPeriod(since, until)
            .subscribe(response => this.statisticsSummary = response,
                () => {
                },
                () => this.isDisabled = false);
    }

    requestPending(): boolean {
        return this.isDisabled;
    }

    proceedToFullData(id: number) {
        console.log(id);
    }

    private determineSinceDate() {
        const date = this.getAppropriateDateFromQueryParam('since');
        return date ? date : moment().startOf('month');
    }

    private determineUntilDate() {
        const date = this.getAppropriateDateFromQueryParam('until');
        return date ? date : moment().endOf('month');
    }

    private getAppropriateDateFromQueryParam(paramName: string): Moment {
        const params = this.route.snapshot.paramMap;
        if (!!params.keys.length) {
            return moment(params[paramName]);
        }
    }

    private getDefaultAccount() {
        this.accountService.getDefaultAccount().subscribe(response => this.account = response);
    }
}
