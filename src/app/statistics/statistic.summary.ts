import {Statistics} from "./statistics";

export class StatisticsSummary {
    totalExpenditureForAllCategories: number;
    statistics: Statistics[]

}