export class Statistics {
    position: number;
    categoryId: number;
    categoryName: string;
    sum: number;
    average: number;
}