import {Injectable} from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Moment} from 'moment';
import {StatisticsSummary} from './statistic.summary';

@Injectable({
    providedIn: 'root'
})
export class StatisticsService {
    private baseUrl = 'api/statistics';

    dateFormat = 'YYYY-MM-DD';

    constructor(private httpClient: HttpClient) {
    }

    findCategoriesWithLabelsAndTheirSumsForGivenPeriod(since: Moment, until: Moment): Observable<StatisticsSummary> {
        const params = new HttpParams()
            .append('since', since.format(this.dateFormat))
            .append('until', until.format(this.dateFormat));
        return this.httpClient.get<StatisticsSummary>(`${this.baseUrl}`, {params: params});
    }
}
