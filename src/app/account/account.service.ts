import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Account} from './account';

@Injectable({
    providedIn: 'root'
})
export class AccountService {
    private baseUrl = 'api/account';

    constructor(private httpClient: HttpClient) {
    }

    getDefaultAccount(): Observable<Account> {
        return this.httpClient.get<Account>(`${this.baseUrl}/default`);
    }

    getOne(id: string): Observable<Account> {
        return this.httpClient.get<Account>(`${this.baseUrl}/${id}`);
    }

    findAll(): Observable<Account[]> {
        return this.httpClient.get<Account[]>(`${this.baseUrl}`);
    }

    add(data: Account): Observable<Account> {
        return this.httpClient.post<Account>(`${this.baseUrl}`, data);
    }

    delete(id: number) {
        return this.httpClient.delete(`${this.baseUrl}/${id}`);
    }

    update(data: Account) {
        return this.httpClient.patch(`${this.baseUrl}/${data.id}`, data);
    }

    makeDefault(id: number) {
        return this.httpClient.patch(`${this.baseUrl}/default/${id}`, {});
    }
}
