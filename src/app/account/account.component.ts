import {Component, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';
import {Account} from './account';
import {AccountService} from './account.service';
import * as _ from 'underscore';

@Component({
    selector: 'app-account',
    templateUrl: './account.component.html'
})
export class AccountComponent implements OnInit {

    accounts: Account[];
    form: FormGroup;
    isDisabled = true;

    constructor(private service: AccountService, private route: ActivatedRoute, private router: Router) {
    }

    ngOnInit() {
        this.initializeForm();
        this.findAll();
        this.findToEditIfDefined();
        this.isDisabled = false;
    }

    submit(): void {
        if (!!this.form.controls.id.value) {
            this.update();
        } else {
            this.add();
        }
    }

    delete(id: number): void {
        this.service.delete(id).subscribe(response => {
            this.findAll();
        });
    }

    getSubmitButtonLabel(): string {
        return !!this.getPathId() ? 'Submit' : 'Add';
    }

    edit(id: number) {
        this.router.navigate([`/account/${id}`]).then(() => {
            if (!!this.getPathId()) {
                window.location.reload();
            }
        });
    }

    requestPending(): boolean {
        return this.isDisabled;
    }

    getPathId() {
        return this.route.snapshot.paramMap.get('id');
    }

    makeDefault(id: number) {
        this.isDisabled = true;
        this.service.makeDefault(id).subscribe(() => {
        }, () => {
        }, () => {
            this.findAll();
        });
    }

    replaceCommaWithDot() {
        this.form.controls.balance.setValue(this.form.controls.balance.value?.toString().replace(',', '.'));
    }

    private initializeForm(dto?: Account): void {
        this.form = new FormGroup({
            'id': new FormControl(dto?.id),
            'name': new FormControl(dto?.name, Validators.required),
            'balance': new FormControl(dto?.balance),
        });
        this.isDisabled = false;
    }

    private findAll(): void {
        this.isDisabled = true;
        this.service.findAll().subscribe(response => {
            this.accounts = _.sortBy(response, 'defaultAccount').reverse();
            this.isDisabled = false;
        });
    }

    private add(): void {
        this.service
            .add(this.form.value)
            .subscribe(() => {
                this.form.reset();
                this.findAll();
            });
    }

    private update(): void {
        this.service
            .update(this.form.value)
            .subscribe(() => {
                this.reloadPage();
            });
    }

    private reloadPage() {
        this.router.navigate(['/account']);
        window.location.reload();
    }

    private findToEditIfDefined() {
        this.isDisabled = true;
        const id = this.getPathId();
        if (id) {
            this.service.getOne(id).subscribe(response => {
                this.initializeForm(response);
            });
        }
    }
}
