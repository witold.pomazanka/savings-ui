import {Component, OnInit} from '@angular/core';
import {LoginService} from "./login.service";
import {AuthenticationService} from "../authentication/authentication.service";
import {Router} from '@angular/router';
import {Login} from "./login";

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

    credentials: Login = new Login();

    constructor(private loginService: LoginService, private authenticationService: AuthenticationService, private router: Router) {
    }

    ngOnInit() {
        document.getElementById('username').focus();
    }

    login(credentials) {
        this.loginService
            .authenticate(credentials)
            .subscribe(response => {
                if (response["username"]) {
                    this.authenticationService.create(response);
                    this.router.navigate(['/']);
                }
            });
    }
}
