import {Injectable} from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';
import {Observable} from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class LoginService {

    constructor(private http: HttpClient) {
    }

    authenticate(credentials): Observable<Object> {
        const body = new HttpParams()
            .set('username', credentials.username)
            .set('password', credentials.password);
        return this.http.post('api/login', body, {headers: {'Content-Type': 'application/x-www-form-urlencoded'}});
    }
}
