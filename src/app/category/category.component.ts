import {Component, OnInit} from '@angular/core';
import {CategoryService} from "./category.service";
import {Category} from "./category";
import {CdkDragDrop, moveItemInArray} from '@angular/cdk/drag-drop';

@Component({
    selector: 'app-category',
    templateUrl: './category.component.html',
    styleUrls: ['./category.component.css']
})
export class CategoryComponent implements OnInit {

    categories: Category[];
    hasChanged: boolean;

    constructor(private service: CategoryService) {
    }

    ngOnInit() {
        this.service.findAll().subscribe(response => {
            this.categories = response;
        });
        this.hasChanged = false;
    }

    drop(event: CdkDragDrop<string[]>) {
        moveItemInArray(this.categories, event.previousIndex, event.currentIndex);
        this.categories.forEach((value, index) => {
            value.position = index;
        });
        this.hasChanged = true;
    }

    updateCategories(): void {
        this.service
            .updateCategories(this.categories)
            .subscribe();
    }
}
