import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Category} from "./category";
import {AddCategory} from "./add.category";

@Injectable({
    providedIn: 'root'
})
export class CategoryService {
    private baseUrl = "api/category";

    constructor(private httpClient: HttpClient) {
    }

    findAll(): Observable<Category[]> {
        return this.httpClient.get<Category[]>(`${this.baseUrl}`)
    }

    add(data: AddCategory): Observable<Category> {
        return this.httpClient.post<Category>(`${this.baseUrl}`, data)
    }

    updateCategories(data: Category[]): Observable<Category> {
        return this.httpClient.put<Category>(`${this.baseUrl}`, data)
    }

    delete(id: number) {
        return this.httpClient.delete<Category>(`${this.baseUrl}/${id}`)
    }
}
