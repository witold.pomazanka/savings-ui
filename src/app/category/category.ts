export class Category {
    id: number;
    name: string;
    position: number;
}
